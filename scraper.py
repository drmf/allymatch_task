import requests
from requests_file import FileAdapter
from bs4 import BeautifulSoup
import os
import pandas as pd

s = requests.Session()
s.mount('file://', FileAdapter())
SUBPAGES_PATH = '/Users/micfel/Documents/allymatch/website_scrape/subpages/'

data_rows = []
for file_name in os.listdir(SUBPAGES_PATH):
    company_name = ""
    company_website = ""
    company_country = ""
    company_description = ""
    company_name = file_name.split(" - Exhibitor Details.html")[0]
    subpage = s.get('file://' + SUBPAGES_PATH + file_name)
    soup = BeautifulSoup(subpage.content, "html.parser")
    company_website_block = soup.find(id="exhibitor_details_website")
    if company_website_block:
        company_website = company_website_block.find("a", href=True).text
    company_address = soup.find(id="exhibitor_details_address")
    if company_address:
        company_country = company_address.find_all("span")[-1].text
    company_details = soup.find(id="exhibitor_details_description")
    if company_details:
        company_description = company_details.find("p").text
    # company_tag_spans = soup.find_all("span", class_="label label-default label-in-list tag-item")
    # if company_tag_spans:
    #     company_tags = [span.text for span in company_tag_spans]
    current_row = {"name": company_name, "website": company_website, "country": company_country,
                   "text_description": company_description}
    data_rows.append(current_row)
df_company = pd.DataFrame(data_rows)
print(df_company["text_description"].map(str.split).map(len))
df_company.to_csv(path_or_buf="company_data.csv", sep="\t", index=False)