import pandas as pd
from sentence_transformers import SentenceTransformer
import re
from numpy import dot
from numpy.linalg import norm
import numpy as np

# Overview: we derive natural language phrases from the given classes, and embed those into a sentence space.
# Then, we embed the company descriptions in the same space.
from sklearn.decomposition import PCA

df_company = pd.read_csv("company_data.csv", sep="\t")
df_company["text_description"] = df_company["text_description"].map(str)
print(len(df_company))
df_company = df_company[df_company["text_description"].map(lambda t: re.sub("s+"," ", t)).map(lambda t: len(t) > 10)]
print(len(df_company))
# df_company = df_company.iloc[:10]
print(df_company)

doc = "NLP  is an interesting     field.  "
new_doc = re.sub("s+"," ", doc)

# the product names in each of the divisions
dp_aerospace = ["Manufacture of engines", "Manufacture of spacecraft and related machinery",
                              "Parts & Spares", "Systems & Software"]
dp_acraft_aviat = ["Airlines", "Manufacture of helicopters", "Manufacture of military fighting vehicles", "Systems & Software",
                         "Parts & Spares", "Manufacture of spacecraft and related machinery", "Manufacture of engines", "Automation systems",
                         "Manufacture of drones (UAV)"]
dp_services = ["Services for Aerospace and Aviation"]
dp_research = ["Research in the field of Aerospace", "Research in the field of Aircraft & Aviation"]
all_divisions_and_products = [dp_aerospace, dp_acraft_aviat, dp_services, dp_research]

def dp_from_dp_code(dp_code):
    if dp_code == "3":
        return "Services for Aerospace and Aviation", None
    else:
        product = all_divisions_and_products[int(dp_code[0]) - 1][int(dp_code[1]) - 1]
        if dp_code.startswith("1"):
            return "Aerospace", product
        if dp_code.startswith("2"):
            return "Aircraft & Aviation", product
        if dp_code.startswith("4"):
            return "Research in the field of Aerospace & Aviation", product


# natural language phrases describing the product names
phrases_aerospace = {"1" + str(i+1): name + " for Aerospace" for (i, name) in enumerate(dp_aerospace)}
phrases_acraft_aviat = {"2" + str(i+1): name + " for Aircraft & Aviation" for (i, name) in enumerate(dp_acraft_aviat)}
phrases_services = {"3": dp_services[0]}
phrases_research = {"4" + str(i+1): name for (i, name) in enumerate(dp_research)}
all_phrases = phrases_aerospace.copy()
all_phrases.update(phrases_acraft_aviat)
all_phrases.update(phrases_services)
all_phrases.update(phrases_research)

div_prod_phrases = [(name, phrase) for name, phrase in all_phrases.items()]
div_prod = [np[0] for np in div_prod_phrases]
phrases = [np[1] for np in div_prod_phrases]


def cosine_similarity(a, b):
    return dot(a, b) / (norm(a) * norm(b))


# embed everything
model_name = "all-mpnet-base-v2"
model = SentenceTransformer(model_name)
phrases_embeddings = model.encode(phrases)

text_description_embeddings = model.encode(df_company["text_description"].values)
print(text_description_embeddings.shape)


# classify companies according to closest embedding of their text_description
company_division = []
company_product = []
for comp_embedding in text_description_embeddings:
    best_div_prod_position = np.argmax([cosine_similarity(comp_embedding, phrases_embeddings[i]) for i in range(len(phrases_embeddings))])
    company_div_prod = div_prod[best_div_prod_position]
    div, prod = dp_from_dp_code(company_div_prod)
    print(div, prod)
    company_division.append(div)
    company_product.append(prod)

df_company["division"] = company_division
print(company_division)
print(df_company["division"])
df_company["product"] = company_product
df_company.to_csv(path_or_buf="company_data_classified.csv", sep="\t", index=False)

# pca = PCA(n_components=2, random_state=42)
# pca_phrases_embeddings = pca.fit_transform(phrases_embeddings)
# sns.scatterplot(x='x', y='y', hue='cluster', data=labeled_embedding(pca_phrases_embeddings, names))
# pca_text_description_embeddings = pca.fit_transform(text_description_embeddings)
# ax = sns.scatterplot(x='x', y='y', data=labeled_embedding(pca_text_description_embeddings, df_company["name"]))
# for i in range(len(pca_text_description_embeddings)):
#     ax.annotate(df_company["name"].iloc[i], pca_text_description_embeddings[i])
# plt.show()
